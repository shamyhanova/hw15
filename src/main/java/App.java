import db.entity.*;
import org.hibernate.Session;
import service.*;

import java.time.LocalDate;
import java.util.List;

public class App {
    ClientService clientService = new ClientService();
    OrderService orderService = new OrderService();
    ManufacturerService manufacturerService = new ManufacturerService();
    OrderDetailsService orderDetailsService = new OrderDetailsService();
    ProductService productService = new ProductService();


    public static void main(String[] args) {
        new App().go();
    }

    public void go() {

       // insertClient();
        insertProduct();
       insertManufacturer();
        //insertOrder();
    }

    private void insertClient() {
        Client client = new Client();
        client.setName("Arystan");
        client.setAddress("Мангилик ел 555");
        clientService.persist(client);

        Client client1 = new Client();
        client1.setName("Anella");
        client1.setAddress("Aulie Ata 77");
        clientService.persist(client1);

        Client client2 = new Client();
        client2.setName("Laura");
        client2.setAddress("Aulie Ata 77");
        clientService.persist(client2);

        Client client3 = new Client();
        client3.setName("Diliya");
        client3.setAddress("Мангилик ел 5");
        clientService.persist(client3);
    }

    private void insertProduct() {
        Product product = new Product();
        product.setName("Iphone");
        product.setPrice(5000);
        productService.persist(product);

        Product product1 = new Product();
        product1.setName("Samsung");
        product1.setPrice(10000);
        productService.persist(product1);

        Product product3 = new Product();
        product3.setName("Iphone");
        product3.setPrice(7000);
        productService.persist(product3);

        Product product4 = new Product();
        product4.setName("Samsung");
        product4.setPrice(60000);
        productService.persist(product4);

    }
    private void insertManufacturer() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Смарт");
        manufacturer.setSiteLink("enu.kz");
        manufacturerService.persist(manufacturer);

        Manufacturer manufacturer1 = new Manufacturer();
        manufacturer1.setName("Умные Телефоны");
        manufacturer1.setSiteLink("enuTele.kz");
        manufacturerService.persist(manufacturer1);

    }
    private void insertOrder() {
        Order order = new Order();

        order.setOrderTime(LocalDate.of(2021, 12, 21));
        orderService.persist(order);

        Order order1 = new Order();

        order.setOrderTime(LocalDate.of(2020, 01, 12));
        orderService.persist(order1);


    }

    


}