package db.dao;


import db.entity.Product;

import java.util.List;

public class ProductDao extends BaseDao<Product, Integer> {
    @Override
    public void persist(Product entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Product entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Product findById(Integer id) {
        return getCurrentSession().get(Product.class, id);
    }


    @Override
    public void delete(Product entity) {
        getCurrentSession().delete(entity);
    }
}