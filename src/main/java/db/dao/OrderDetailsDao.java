package db.dao;
import db.entity.OrderDetails;

public class OrderDetailsDao extends BaseDao<OrderDetails, Integer> {
    @Override
    public void persist(OrderDetails entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(OrderDetails entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public OrderDetails findById(Integer id) {
        return getCurrentSession().get(OrderDetails.class, id);
    }


    @Override
    public void delete(OrderDetails entity) {
        getCurrentSession().delete(entity);
    }
}