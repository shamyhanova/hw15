package db.dao;


import db.entity.Order;

import java.util.List;

public class OrderDao extends BaseDao<Order, Integer> {
    @Override
    public void persist(Order entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Order entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Order findById(Integer id) {
        return getCurrentSession().get(Order.class, id);
    }


    @Override
    public void delete(Order entity) {
        getCurrentSession().delete(entity);
    }
}