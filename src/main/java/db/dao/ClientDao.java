package db.dao;

import db.entity.Client;

import java.util.List;

public class ClientDao extends BaseDao<Client, Integer> {
    @Override
    public void persist(Client entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Client entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Client findById(Integer id) {
        return getCurrentSession().get(Client.class, id);
    }


    @Override
    public void delete(Client entity) {
        getCurrentSession().delete(entity);
    }
}