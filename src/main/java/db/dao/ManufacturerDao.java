package db.dao;
import db.entity.Manufacturer;

public class ManufacturerDao extends BaseDao<Manufacturer, Integer> {
    @Override
    public void persist(Manufacturer entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Manufacturer entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Manufacturer findById(Integer id) {
        return getCurrentSession().get(Manufacturer.class, id);
    }

    @Override
    public void delete(Manufacturer entity) {
        getCurrentSession().delete(entity);
    }
}