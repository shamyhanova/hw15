package db.entity;

import javax.persistence.*;

@Entity
@Table(name = "manufacturer")

public class Manufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(name = "Name")
    String Name;

    @Column(name = "Site_Link")
    String SiteLink;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSiteLink() {
        return SiteLink;
    }

    public void setSiteLink(String SiteLink) {
        this.SiteLink = SiteLink;
    }


}

