package service;
import db.dao.OrderDao;
import db.dao.Dao;
import db.entity.Order;


public class OrderService {
    private static Dao<Order, Integer> orderDao;


    public OrderService() {

        orderDao = new OrderDao();
    }
    public void persist(Order entity) {
        orderDao.openSession();
        orderDao.persist(entity);
        orderDao.closeSession();
    }

    public Order findById(Integer id) {
        orderDao.openSession();
        Order emp = orderDao.findById(id);
        orderDao.closeSession();
        return emp;
    }

    public void delete(Integer id) {
        orderDao.openSession();
        Order emp = orderDao.findById(id);

        orderDao.delete(emp);
        orderDao.closeSession();
    }

    public void update(Order order) {
        orderDao.openSession();
        orderDao.update(order);
        orderDao.closeSession();
    }
}
