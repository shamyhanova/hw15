package service;
import db.dao.ManufacturerDao;
import db.dao.Dao;
import db.entity.Manufacturer;

public class ManufacturerService {
    private static Dao<Manufacturer, Integer> manufacturerDao;


    public ManufacturerService() {

        manufacturerDao = new ManufacturerDao();
    }
    public void persist(Manufacturer entity) {
        manufacturerDao.openSession();
        manufacturerDao.persist(entity);
        manufacturerDao.closeSession();
    }

    public Manufacturer findById(Integer id) {
        manufacturerDao.openSession();
        Manufacturer manufacturer = manufacturerDao.findById(id);
        manufacturerDao.closeSession();
        return manufacturer;
    }

    public void delete(Integer id) {
        manufacturerDao.openSession();
        Manufacturer manufacturer = manufacturerDao.findById(id);

        manufacturerDao.delete(manufacturer);
        manufacturerDao.closeSession();
    }

    public void update(Manufacturer manufacturer) {
        manufacturerDao.openSession();
        manufacturerDao.update(manufacturer);
        manufacturerDao.closeSession();
    }
}
