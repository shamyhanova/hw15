package service;
import db.dao.ProductDao;
import db.dao.Dao;
import db.entity.Product;


public class ProductService {
    private static Dao<Product, Integer> productDao;


    public ProductService() {

        productDao = new ProductDao();
    }
    public void persist(Product entity) {
        productDao.openSession();
        productDao.persist(entity);
        productDao.closeSession();
    }

    public Product findById(Integer id) {
        productDao.openSession();
        Product emp = productDao.findById(id);
        productDao.closeSession();
        return emp;
    }

    public void delete(Integer id) {
        productDao.openSession();
        Product product = productDao.findById(id);

        productDao.delete(product);
        productDao.closeSession();
    }

    public void update(Product order) {
        productDao.openSession();
        productDao.update(order);
        productDao.closeSession();
    }
}
