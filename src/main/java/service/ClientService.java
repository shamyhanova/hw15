package service;

import db.dao.ClientDao;
import db.dao.Dao;
import db.entity.Client;

public class ClientService {
    private static Dao<Client, Integer> clientDao;

    public ClientService() {
        clientDao = new ClientDao();
    }

    public void persist(Client entity) {
        clientDao.openSession();
        clientDao.persist(entity);
        clientDao.closeSession();
    }

    public Client findById(Integer id) {
        clientDao.openSession();
        Client emp = clientDao.findById(id);
        clientDao.closeSession();
        return emp;
    }

    public void delete(Integer id) {
        clientDao.openSession();
        Client client = clientDao.findById(id);

        clientDao.delete(client);
        clientDao.closeSession();
    }

    public void update(Client client) {
        clientDao.openSession();
        clientDao.update(client);
        clientDao.closeSession();
    }
}
