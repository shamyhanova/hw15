package service;
import db.dao.OrderDao;
import db.dao.Dao;
import db.dao.OrderDetailsDao;
import db.entity.Order;
import db.entity.OrderDetails;


public class OrderDetailsService {
    private static Dao<OrderDetails, Integer> orderDetailsDao;


    public OrderDetailsService() {

        orderDetailsDao = new OrderDetailsDao();
    }
    public void persist(OrderDetails entity) {
        orderDetailsDao.openSession();
        orderDetailsDao.persist(entity);
        orderDetailsDao.closeSession();
    }

    public OrderDetails findById(Integer id) {
        orderDetailsDao.openSession();
        OrderDetails orderDetails = orderDetailsDao.findById(id);
        orderDetailsDao.closeSession();
        return orderDetails;
    }

    public void delete(Integer id) {
        orderDetailsDao.openSession();
        OrderDetails orderDetails = orderDetailsDao.findById(id);

        orderDetailsDao.delete(orderDetails);
        orderDetailsDao.closeSession();
    }

    public void update(OrderDetails orderDetails) {
        orderDetailsDao.openSession();
        orderDetailsDao.update(orderDetails);
        orderDetailsDao.closeSession();
    }
}
