CREATE TABLE client(
ID serial primary key,
Name varchar(30),
Address VARCHAR(60)
);
CREATE TABLE n_order(
ID serial primary key,
client_Id int,
Order_time DATE,
FOREIGN KEY (client_Id) REFERENCES client(ID)
);

create table manufacturer(
ID serial primary key unique,
Name varchar(30),
Site_Link varchar(30));

create table product(
ID serial primary key,
Name varchar(30),
Price int,
manufacturer_id int unique

);
CREATE TABLE Order_Details(
Order_Id int,
product_Id int,
product_Amount INT,
FOREIGN KEY (Order_Id) REFERENCES n_order(ID),
FOREIGN KEY (product_Id) REFERENCES product(ID)
);